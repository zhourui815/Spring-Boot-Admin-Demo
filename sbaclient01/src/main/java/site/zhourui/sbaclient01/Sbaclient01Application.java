package site.zhourui.sbaclient01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sbaclient01Application {

    public static void main(String[] args) {
        SpringApplication.run(Sbaclient01Application.class, args);
    }

}
